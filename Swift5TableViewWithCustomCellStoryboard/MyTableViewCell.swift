////  MyTableViewCell.swift
//  Swift5TableViewWithCustomCellStoryboard
//
//  Created on 30/10/2020.
//  
//

import UIKit

class MyTableViewCell: UITableViewCell {

    @IBOutlet var label: UILabel!
    @IBOutlet var imageV: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
